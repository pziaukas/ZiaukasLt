---
layout: default
---

### {% include "icon/handshake-solid.svg" %} Hello

My name is Pranas Ziaukas but you can just call me Frank.

I enjoy playing [table tennis]({{ external.pingpong }}) with friends, doing [photography]({{ external.photography }}) and looking for hidden lunch boxes around the world (also known as [geocaching]({{ external.geocaching }})).

During workdays I'm a software and data engineer and a mathematician among other roles.


### {% include "icon/graduation-cap-solid.svg" %} Background

Having studied in Lithuania and Sweden, I obtained Master's degree in mathematics. Feel free to read [my thesis]({{ external.thesis }}).

Also, I happened to co-author a handful of [scientific papers]({{ external.papers }}) whilst working in academia.


### {% include "icon/briefcase-solid.svg" %} Career

#### {% include "icon/laptop-code-solid.svg" %} Coding
* Python, R, SQL, Bash  
data engineering and science ({{ 2016 | yearsSince }} years)
* TypeScript / JavaScript, C#, HTML / CSS  
software engineering and development ({{ 2015 | yearsSince }} years)
* MATLAB, SAS, LaTeX  
academical research (3 years)

#### {% include "icon/brain-solid.svg" %} Interests
* mathematics, algorithms
* artificial intelligence, machine learning
* big data modelling, analysis
* automation


### {% include "icon/envelope-solid.svg" %} Get in touch

In case you have any ideas or feedback, feel free to drop me a message.

<form name="ziaukas-lt-get-in-touch" method="POST" data-netlify="true" action="/success.html">

<label for="message">Message</label>
<textarea id="message" name="message" placeholder="Type your message here..." required></textarea>
<label for="contact">Contact details</label>
<input type="text" id="contact" name="contact" placeholder="Phone, e-mail, etc." required>

<input type="submit" value="Send">

</form>
