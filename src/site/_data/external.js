module.exports = {
  "cv" : "https://docs.google.com/viewer?url=https://gitlab.com/pziaukas/CurriculumVitae/-/jobs/artifacts/master/raw/build/Ziaukas_CV.pdf?job=CV%20Builder",
  "linkedin" : "https://www.linkedin.com/in/pranasziaukas",
  "gitlab" : "https://gitlab.com/pziaukas",

  "pingpong" : "https://pingpong.ziaukas.lt",
  "photography" : "https://500px.com/pranas",
  "geocaching" : "https://project-gc.com/ProfileStats/PranSima",

  "thesis" : "https://docs.google.com/viewer?url=https://gitlab.com/pziaukas-repos/Thesis/ThesisMaster/raw/master/build/thesis.pdf",
  "papers" : "https://gitlab.com/pziaukas/ResearchPapers/blob/master/README.md"
}
