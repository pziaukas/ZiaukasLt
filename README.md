# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [1.2.3](https://gitlab.com/pziaukas/ZiaukasLt/compare/v1.2.2...v1.2.3) (2020-07-07)


### Bug Fixes

* **body:** typo in 'engineering' ([57bf90c](https://gitlab.com/pziaukas/ZiaukasLt/commit/57bf90c))



### [1.2.2](https://gitlab.com/pziaukas/ZiaukasLt/compare/v1.2.1...v1.2.2) (2019-06-28)


### Bug Fixes

* **contact-form:** success.html references background grid correctly ([54c763d](https://gitlab.com/pziaukas/ZiaukasLt/commit/54c763d))



### [1.2.1](https://gitlab.com/pziaukas/ZiaukasLt/compare/v1.1.0...v1.2.1) (2019-06-28)


### Bug Fixes

* **meta:** absolute image url ([5ce12f8](https://gitlab.com/pziaukas/ZiaukasLt/commit/5ce12f8))


### Features

* contact form (get in touch) ([cf4c8cb](https://gitlab.com/pziaukas/ZiaukasLt/commit/cf4c8cb))
* netlify redirects ([1032936](https://gitlab.com/pziaukas/ZiaukasLt/commit/1032936))



## [1.1.0](https://gitlab.com/pziaukas/ZiaukasLt/compare/v1.0.0...v1.1.0) (2019-06-17)


### Bug Fixes

* **footer:** removed a dead link to changelog ([2e9b2c0](https://gitlab.com/pziaukas/ZiaukasLt/commit/2e9b2c0))


### Features

* design according to the personal style guidelines ([14f00a7](https://gitlab.com/pziaukas/ZiaukasLt/commit/14f00a7))
* providing meta image for the site ([8136859](https://gitlab.com/pziaukas/ZiaukasLt/commit/8136859))



## [1.0.0](https://gitlab.com/pziaukas/ZiaukasLt/compare/v0.6.0...v1.0.0) (2019-06-12)


### Bug Fixes

* **header:** linking cv directly due to double redirect (?) bug ([e33e49b](https://gitlab.com/pziaukas/ZiaukasLt/commit/e33e49b))


### Features

* directing traffic through www.ziaukas.lt ([01d6775](https://gitlab.com/pziaukas/ZiaukasLt/commit/01d6775))
* opening portfolio links in new tabs automatically ([d31fcfe](https://gitlab.com/pziaukas/ZiaukasLt/commit/d31fcfe))


### BREAKING CHANGES

* custom domain www.ziaukas.lt is now in action instead of the netlify subdomain



## [0.6.0](https://gitlab.com/pziaukas/ZiaukasLt/compare/v0.5.0...v0.6.0) (2019-06-11)


### Bug Fixes

* temporarily disabling the css minification due to paragraph glitch ([ba00e9d](https://gitlab.com/pziaukas/ZiaukasLt/commit/ba00e9d))
* **header:** hiding the header until the design is decided ([850f925](https://gitlab.com/pziaukas/ZiaukasLt/commit/850f925))
* using rel="noopener noreferrer" for safety ([32bd22b](https://gitlab.com/pziaukas/ZiaukasLt/commit/32bd22b))


### Features

* minifying output ([c27a6f0](https://gitlab.com/pziaukas/ZiaukasLt/commit/c27a6f0))
* offloaded generator config into .eleventy.js ([e9d6f61](https://gitlab.com/pziaukas/ZiaukasLt/commit/e9d6f61))
* **footer:** feeding dynamic data from _data and package.json ([fbafc05](https://gitlab.com/pziaukas/ZiaukasLt/commit/fbafc05))



## 0.5.0 (2019-06-11)


### Features

* using standard-version tool ([dd2fd2d](https://gitlab.com/pziaukas/ZiaukasLt/commit/dd2fd2d))
