module.exports = function(config) {
  config.addLayoutAlias('default', 'layout/base.njk');
  
  config.addPassthroughCopy("./src/site/img");
  config.addPassthroughCopy("./src/site/favicon.ico");

  config.addTransform("htmlmin", require("./src/utils/minify-html.js"));

  let markdownIt = require("markdown-it");
  let markdownItOptions = {
    html: true
  };
  let mila = require("markdown-it-link-attributes");
  let milaOptions = {
    attrs: {
      target: "_blank",
      rel: "noopener noreferrer"
    }
  };
  let markdownLib = markdownIt(markdownItOptions).use(mila, milaOptions);
  config.setLibrary("md", markdownLib);

  config.addFilter("yearsSince", function(year) {
    return `${new Date().getFullYear() - year}+`;
  });
  
  return {
    dir: {
      input: "./src/site",
      output: "./public"
    },
    templateFormats : ["njk", "md"],
    htmlTemplateEngine : "njk",
    markdownTemplateEngine : "njk",
    passthroughFileCopy: true
  };
};
